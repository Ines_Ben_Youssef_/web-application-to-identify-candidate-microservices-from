# Web application to identify candidate microservices from monolith

This web application provides support to identify possible microservices from a monolith Java project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

In order to run the web application correctly, you need :

1)A web browser.

2)Java installed on your machine.

## Running the web application

In order to run the microservice :

1)Run the microservice that contains the identification process. It's present in the same project.

2)Provide the folder path and select the algorithm and display choices.

The project folder path must respect this format :
"C:\\\Users\\\user\\\path\\\folder_name"

3)Proceed

## Testing the web application

To test the web application, you can access test different combinations of algorithms and displays. 

At each test, you must wait for the identification microservice to send the response to the web application.

To export the results, click on generate results.

To navigate between the possible alternatives, use the next/previous solutions buttons.

To interact with the solution, you can drag and drop the classes from one cluster to another.
