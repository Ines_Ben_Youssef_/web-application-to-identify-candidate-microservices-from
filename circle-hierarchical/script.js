var margin = 100,
  diameter = 600;

  function encode( s ) {
    var out = [];
    for ( var i = 0; i < s.length; i++ ) {
        out[i] = s.charCodeAt(i);
    }
    return new Uint8Array( out );
}

var button = document.getElementById( 'button' );
button.addEventListener( 'click', function() {
 
var xhr = new XMLHttpRequest();
var url = "http://localhost:9091/Hierarchy";
xhr.open("POST", url, true);
xhr.setRequestHeader("Content-Type", "application/json");
xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);
        console.log(json);
    }
};
var data = JSON.stringify(clusters);
xhr.send(data);

  console.log("new clusters ",clusters);
  var data = encode( JSON.stringify(clusters, null, 4) );

    var blob = new Blob( [ data ], {
        type: 'application/octet-stream'
    });
    
    url = URL.createObjectURL( blob );
    var link = document.createElement( 'a' );
    link.setAttribute( 'href', url );
    link.setAttribute( 'download', 'results.json' );
    
    var event = document.createEvent( 'MouseEvents' );
    event.initMouseEvent( 'click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
    link.dispatchEvent( event );


  }

);

var clusters ;

var color = d3.scale.linear()
  .domain([-1, 5])
  .range(["#F7F7F7", "hsl(228,30%,40%)"])
  .interpolate(d3.interpolateHcl);


  var tooltip = d3.tip()
                        .attr('class', 'd3-tip')
                          .offset([-10, 0])
                        .html(function(d) {
                         return " <span style='color:#fff'>" + d.name + "</span>";
                     })

                     var tooltip2 = d3.tip()
                     .attr('class', 'd3-tip')
                       .offset([-10, 0])
                     .html(function(d) {
                      return  " <span style='color:#fff'>" + d.name 
                      +" <br>"+"Fitness function : "+ d.fitness+ "</span>";
                  })


var pack = d3.layout.pack()
  .padding(2)
  .size([diameter - margin, diameter - margin])
  .value(function(d) {
    return ( 100 );
  })

var svg = d3.select("body").append("svg")
  .attr("width", window.innerWidth)
  .attr("height", diameter)
  .append("g")
  .attr("transform", "translate(" + window.innerWidth / 2 + "," + diameter / 2 + ")");

  svg.call(tooltip);
  svg.call(tooltip2);

d3.json("http://localhost/final/circle-hierarchical/circle_hierarchy.json", function(error, root) {

  d3.json("http://localhost:9091/Hierarchy", function(error2, root2) {

  if (error2) throw error2;
  });

  var x = document.getElementById("wait");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }

  clusters = JSON.parse(JSON.stringify(root));

  screen(clusters);
  console.log("clusters ", clusters);

  var nodes = pack.nodes(root);

  var focus = root ;

  var drag = d3.behavior.drag()

    .on("drag", function(d, i) {
      d.x += d3.event.dx;
      d.y += d3.event.dy;

      draw2();})

    .on("dragend", function(d, i) {
    //console.log ( root );
 
   
    var x2 = d.x ;
    var y2 = d.y ;
    var r2 = d.r ;
    var cluster = null ;
    var min = 1000000 ;
    nodes.forEach(element => {
      
      var x1 = element.x ;
      var y1 = element.y ;
      var r1= element.r ;

      distSq = Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))) ;
      
      
      if ( distSq + r2 < r1 &&  ( element.children !== undefined ) ){
        if ( min > distSq + r2 )
        {
          min = distSq + r2 ;
          cluster = element ;
        }
      }



    });



    
    var nodename = d.name ;
    var oldparentname = d.parent.name;
    var newparentname = cluster.name ;
    var child = allDescendants( root , nodename );
    var oldparent = allDescendants( root , oldparentname );
    var newparent = allDescendants( root , newparentname );
    child.r = d.r ;
    
    
    
    child.parent = newparent ;
    update_cluster(child,oldparent,newparent);
    newparent.children.push(child);
    var index = oldparent.children.indexOf(child);
    if (index > -1) {
      oldparent.children.splice(index, 1);
    }

    updateVis() 
  });

  var update_cluster = function( a , b , c ){

    var ch = allDescendants(clusters,a.name);
    var o = allDescendants(clusters,b.name);
    var p = allDescendants(clusters,c.name);
 

    p.children.push(ch);
    
    var indexx
    for(i=0; i < o.children.length; i++){
      if ( o.children[i].name == a.name )
      {
        const index = i ;

        indexx = i ;
      }


 }
 if (indexx > -1) {
  o.children.splice(indexx, 1);
}

  
}


  var circle = svg.selectAll("circle")
    .data(nodes)
    .enter().append("circle")
    .attr("class", function(d) {
      return d.parent ? d.children ? "node node--middle" : "node node--leaf" : "node node--root";
    })
    .style("fill", function(d) {
      if ( d.clustertype == "main cluster")
      {console.log(d.name);
        return "#36486b" ;
      }
      else
      return d.children ? color(d.depth) : null;
    });


    circle.on("mouseover", function(d){
  

      if(!d.children){
        var sel = d3.select(this);
        sel.moveToFront();
        tooltip.show(d)
     }
     else
     {
      tooltip2.show(d) ;
     }
      
 }).on("mouseout", function(d){
  

  if(!d.children){
    tooltip.hide(d);
 }
 else{
  tooltip2.hide(d) ;
 }
  
})


   
    d3.selection.prototype.moveToFront = function() {
      return this.each(function(){
        this.parentNode.appendChild(this);
      });
    };

/*
  var text = svg.selectAll("text")
    .data(nodes)
    .enter().append("text")
    .attr("class", "label")
    .style("fill-opacity", function(d) {
      return d.children !== undefined ? 1 : 0;
    })
    .style("display", function(d) {
      return d.parent === root ? "inline" : "none";
    })
    .text(function(d) {
      return d.name;
    });
*/
  var node = svg.selectAll("circle,text");

 

  

  svg.selectAll(".node--leaf").call(drag);

  d3.select("body")
    .style("background", color(-1))

  draw();

  function allDescendants ( k , d ) {
    
    if(k.name == d){
      return k ;
 }else if (k.children != null){
      var i;
      var result = null;
      for(i=0; result == null && i < k.children.length; i++){
           result = allDescendants(k.children[i], d);
      }
      return result;
 }
 return null;
}
  

  function draw() {
    var k = diameter / ((root.r+2) * 2 + margin);
    
    node.transition()
    .duration(5000).attr("transform", function(d) {
      return "translate(" + (d.x - root.x) * k + "," + (d.y - root.y) * k + ")";
    });
    circle.attr("r", function(d) {
      return (d.r * k + 2 );
    });
  }

  
  function draw2() {
    var k = diameter / ((root.r+2) * 2 + margin);
    
    node.attr("transform", function(d) {
      return "translate(" + (d.x - root.x) * k + "," + (d.y - root.y) * k + ")";
    });
    circle.attr("r", function(d) {
      return d.r * k+ 2;
    });
  }

  var updateVis = function (){
    
    var data1 = pack.nodes(root);
    draw();
   
  }

  
});


function screen ( k ) {
    
  if(k.children == undefined){
    k.type= "node";
    
}

else if (k.children != null){
  k.type ="cluster" ;
    var i;
   
    for(i=0;  i < k.children.length; i++){
        screen(k.children[i]);
    }
   
}

}